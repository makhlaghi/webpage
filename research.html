<!DOCTYPE html>
<!--
  Mohammad Akhlaghi's personal webpage.
  Copyright (C) 2016-2025 Mohammad Akhlaghi

  This HTML source is free software: you can redistribute it and/or
  modify it under the terms of the GNU General Public Licence as
  published by the Free software foundation, either version 3 of the
  License, or (at your option) any later version.

  This HTML source is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  A copy of the GNU General Public License is available at
  <http://www.gnu.org/licenses>.
-->

<html lang="en-US">

  <head>
    <title>Mohammad Akhlaghi -- Research -- Centro de Estudios de Física del Cosmos de Aragón</title>

    <meta charset="UTF-8">
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8">

    <link rel="shortcut icon" href="./img/cefca-icon.png" />

    <meta name="viewport"
	  content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/fonts.css">
    <link rel="stylesheet" href="css/base.css">
    <link rel="stylesheet" media="only screen and (min-width: 32em)"
	  href="css/wide.css">
  </head>

  <body>
    <div class="page">



      <!-- Page top and navigation -->
      <header role="banner">
	<nav role="navigation">
	  <ul>
	    <li>MOHAMMAD AKHLAGHI</li>
	    <li><a href="index.html">Welcome</a></li>
	    <li><a href="blog.html">Blog</a></li>
	  </ul>
	</nav>
      </header>



      <!-- Container keeping main and sidebar -->
      <div class="container clearfix">


	<!-- Main body of page -->
	<main role="main">
	  <article>
	    <section class="title-abstract">
	      <h1>Research</h1>

	      <p>I am an observational astronomer and mainly
	      interested in
	      studying <a href="https://en.wikipedia.org/wiki/Galaxy_formation_and_evolution">Galaxy
	      evolution</a> from
	      the <a href="https://en.wikipedia.org/wiki/Reionization">epoch
	      of reionization</a> until now. My experiences until now
	      have mainly been focused in
	      the <a href="https://en.wikipedia.org/wiki/Visible-light_astronomy">optical</a>
	      and near infra-red spectral ranges. For my research, I
	      extensively use data
	      from <a href="https://en.wikipedia.org/wiki/Hubble_Space_Telescope">Hubble
	      Space Telescope</a> (HST, shown below from Wikipedia)
	      data along with ground based data and in particular with
	      the <a href="https://en.wikipedia.org/wiki/Multi_Unit_Spectroscopic_Explorer">MUSE
	      camera</a> (shown below, picture by Roland Bacon) on
	      the <a href="https://en.wikipedia.org/wiki/Very_Large_Telescope">Very
	      Large Telescope</a> along with many other ground based
	      instruments when necessary.</p>

	      <img src="img/muse.jpg" width=49% />
	      <img src="img/hst.jpg" width=49% />

	      <p>&nbsp;</p>
	      <p>The generality of my research interests/motives can
		mainly be summarized in this nice quotation
		from <a href="https://en.wikipedia.org/wiki/Karl_Popper">Karl
		Popper</a>: "I am interested in science and in
		philosophy only because I want to learn something
		about the riddle of the world in which we live, and
		the riddle of man's knowledge of that
		world". Therefore I am mostly interested in the
		low-level aspects of astronomy (where our knowledge
		and the raw/noisy data merge). This interest lead me
		to the study and discovery of a
		fundamentally <a href="https://arxiv.org/abs/1505.01664">new
		paradigm</a> to detect very faint and diffuse signal
		(distant galaxies) in noisy datasets (telescope
		images) and a new way to <i>man</i>eage data
		lin<i>eage</i>
		(Maneage: <a href="http://maneage.org">maneage.org</a>)
		for an exactly reproducilble workflow/lineage that is
		easy to publish and allows longevity.</p>

	      <p>I am now using this new detection method for the
		study of intergalactic gas between the cosmic web
		filaments, very distant star forming galaxies known as
		Lyman-break galaxies, along with some work on the low
		surface brightness regions of closer galaxies at lower
		redshifts.</p>

	    </section>
	  </article>

	</main>


	<!-- Sidebar -->
	<div class="sidebar">

	  <aside role="complementary" class="importantaside">
	    <h1>REPRODUCIBLE SCIENCE</h1>
	    <p>See <a href="https://arxiv.org/abs/2006.03018">arXiv:2006.03018</a>
	    describing the necessity of reproducible research and that
	    they must consider longevity.</p>
	  </aside>

	  <aside role="complementary">
	    <h1>MANEAGE (MANAGE DATA LINEAGE)</h1>
	    <p>Maneage (<a href="https://maneage.org">maneage.org</a>)
	    is a highly customizable framework, allowing full control
	    over data lineage that is already used in the publications
	    of this page.</p>
	  </aside>

	  <aside role="complementary">
	    <h1>PUBLICATIONS</h1>
	      <figure>
		<a href="https://arxiv.org/a/akhlaghi_m_1.html">
		  <img src="./img/arxiv.png" width=45% /></a>
		<a href="http://adsabs.harvard.edu/cgi-bin/nph-abs_connect?db_key=AST&db_key=PRE&qform=AST&arxiv_sel=astro-ph&aut_logic=OR&obj_logic=OR&author=Akhlaghi%2C+Mohammad&object=&start_mon=&start_year=&end_mon=&end_year=&ttl_logic=OR&title=&txt_logic=OR&text=&nr_to_return=200&start_nr=1&jou_pick=ALL&ref_stems=&data_and=ALL&group_and=ALL&start_entry_day=&start_entry_mon=&start_entry_year=&end_entry_day=&end_entry_mon=&end_entry_year=&min_score=&sort=SCORE&data_type=SHORT&aut_syn=YES&ttl_syn=YES&txt_syn=YES&aut_wt=1.0&obj_wt=1.0&ttl_wt=0.3&txt_wt=3.0&aut_wgt=YES&obj_wgt=YES&ttl_wgt=YES&txt_wgt=YES&ttl_sco=YES&txt_sco=YES&version=1">
		  <img src="./img/ads.png" width=45% /></a>
	      </figure>
	  </aside>

	</div>

	<!-- End of the main and sidbar container -->
      </div>


      <!-- Page footer -->
      <footer role="contentinfo">
<address>Centro de Estudios de Física del Cosmos de Aragón, Plaza San Juan 1, Planta 2, Teruel, Spain.</address>
	<copyright>Copyright &copy; 2016-2025 Mohammad Akhlaghi
	  (<a href="mailto:Mohammad
		    Akhlaghi<mohammad|at|akhlaghi.org>">mohammad::at::akhlaghi.org</a>).</copyright>
      </footer>

    </div>
  </body>

</html>
