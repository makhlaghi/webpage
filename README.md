# Source and history of Mohammad Akhlaghi's personal webpage

This repository contains the version controlled history of my webpage
(at http://www.akhlaghi.org). Since the files are public it is
necessary to have the older versions available.