<!DOCTYPE html>
<!--
  Mohammad Akhlaghi's personal webpage.
  Copyright (C) 2018-2025 Mohammad Akhlaghi <mohammad@akhlaghi.org>

  This HTML source is free software: you can redistribute it and/or
  modify it under the terms of the GNU General Public Licence as
  published by the Free software foundation, either version 3 of the
  License, or (at your option) any later version.

  This HTML source is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  A copy of the GNU General Public License is available at
  <http://www.gnu.org/licenses>.
-->

<html lang="en-US">

  <head>
    <title>Reproducible science -- Mohammad Akhlaghi -- Centro de Estudios de Física del Cosmos de Aragón</title>

    <meta charset="UTF-8">
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8">

    <link rel="shortcut icon" href="./img/cefca-icon.png" />

    <meta name="viewport"
	  content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/fonts.css">
    <link rel="stylesheet" href="css/base.css">
    <link rel="stylesheet" media="only screen and (min-width: 32em)"
	  href="css/wide.css">
  </head>

  <body>
    <div class="page">



      <!-- Page top and navigation -->
      <header role="banner">
	<nav role="navigation">
	  <ul>
	    <li>MOHAMMAD AKHLAGHI</li>
	    <li><a href="index.html">Welcome</a></li>
	    <li><a href="blog.html">Blog</a></li>
	  </ul>
	</nav>
      </header>



      <!-- Container keeping main and sidebar -->
      <div class="container clearfix">


	<!-- Main body of page -->
	<main role="main">
	  <article>
	    <section class="title-abstract">
	      <h1>Reproducible science/research</h1>

              <p>The Tsunami of data that has engulfed astronomers in
		the last two decades, combined with faster processors
		and faster internet connections has made it much more
		easier to obtain a result. However, these factors have
		also increased the complexity of a scientific
		analysis, such that it is no longer possible to
		describe all the steps of an analysis in the published
		paper. Citing this difficulty, many authors suffice to
		describing the generalities of their analysis in their
		papers.</p>

	      <p>It is impossible to study the validity a result if
		you can't reproduce it. The complexity of modern
		science makes it vitally important to <i>exactly</i>
		reproduce the final result (given the same raw input
		data). Even a small deviation can be due to many
		different parts of an analysis. For example,
		see <a href="https://physicstoday.scitation.org/do/10.1063/PT.6.1.20180822a/full/">this
		review</a> of a 7-year old conflict in theoretical
		condensed matter which was only identified after the
		relative codes were shared. Also
		see <a href="http://www.sciencemag.org/news/2018/07/plan-replicate-50-high-impact-cancer-papers-shrinks-just-18">this
		report</a>, on how a project aiming to reproduce 50
		high-impact cancer papers shrinks to just 18.</p>

	      <p>Nature is already a black box which we are trying so
		hard to comprehend. Not letting other scientists see
		the exact steps taken to reach a result, or not
		allowing them to modify it (do experiments on it) is a
		self-imposed black box, which only exacerbates our
		ignorance.</p>

	      <p>To better highlight the importance of
		reproducibility, consider this analogy: the English
		style of scientific papers. Why do non-English
		speaking researchers have to invest a lot of time and
		energy in mastering English to a sufficiently high
		level for publishing their exciting results?  Using an
		online translator is enough to convey the absolute
		final result of their analysis. For example that
		galaxies grow as a specific function of the age of the
		universe. Everyone will get the ultimate point they
		want to make through a poor translation. Then, why do
		journals request that the paper be written a very good
		English level?</p>

	      <p>It is because journals do not publish raw
		results. Understanding the <i>method</i> that the
		result was obtained is more important than the result
		itself. Good language (English in this case)
		standardizes the reading and allows the readers to
		understand the details of the method more easily and
		focus on the details. Otherwise, the readers have
		waste a lot of mental energy on what a mis-spelled
		word, or poorly written sentence (bad grammar/style)
		may mean in interpreting the result.</p>

	      <p>Exactly the same logic applies to reproducibility:
		without sufficient standards, readers cannot focus on
		the details, and will make different
		interpretations. Just as non-English speakers are
		forced to master English, people that are not trained
		in software MUST invest the time and energy to do
		so. You hear this statement a lot from many
		scientists: "software is not my specialty, I am not a
		software engineer, so the quality of my
		code/processing doesn't matter. Why should I master
		good coding style (or release my code), when I am
		hired to do Astronomy/Biology?". This is akin to a
		French scientist saying that "English is not my
		language, I am not Shakespeare. So the quality of my
		English writing doesn't matter. Why should I master
		good English style, when I am hired to do
		Astronomy/Biology?". </p>

	      <p>Other scientists should be able to reproduce, check
                and experiment on the results of anything that is to
                carry the "scientific" label. Any result that is not
                reproducible (due to incomplete information by the
                author) is not scientific: the readers have to have
                faith in the subjective experience of the authors in
                the very important choice of configuration values and
                order of operations: this is contrary to the
                definition of science.</p>

	      <p>This topic is recently gaining attention in the
		community. The National Academies of Sciences,
		engineering and medicine recently published
		a <a href="https://www.nap.edu/catalog/25116/open-science-by-design-realizing-a-vision-for-21st-century">very
		complete report</a> on the necessity of "Open science"
		along with guidelines and recommendations on how to
		implement it. The Nature journal's editorial board
		also recently
		<a href="https://www.nature.com/articles/d41586-018-02741-4">announced</a>
		their
		<a href="https://www.nature.com/authors/policies/availability.html#code">new
		policy</a> regarding software and methods in their
		published papers along with
		a <a href="http://www.nature.com/documents/GuidelinesCodePublication.pdf">Code
		and software submission checklist</a>. Here is an
		excerpt from the new policy:</p>

	      <blockquote cite="https://www.nature.com/authors/policies/availability.html#code">
		<i>Authors must make available upon request, to editors
		and reviewers, any previously unreported custom
		computer code or algorithm used to generate results
		that are reported in the paper and central to its main
		claims. Any reason that would preclude the need for
		code or algorithm sharing will be evaluated by the
		editors who reserve the right to decline the paper if
		important code is unavailable.</i>
	      </blockquote>

	      <p>A similar policy was also introduced in the Science
		journal since
		2011. <a href="http://www.pnas.org/content/early/2018/03/08/1708290115.short">Stodden
		et al. (2018)</a> have done a very interesting study
		on the effectiveness of this policy change and how
		authors share their data and processing. Unfortunately
		the results aren't too encouraging: Only ~10% of the
		papers they reviewed provided links to their data and
		scripts (enabling reproducibility without
		contact). Including those that gave the scripts after
		private contacts, this paper only deems 26% of the
		papers reproducible (those that share sufficient
		details of the method). They also compare with the
		period before the policy and see a slight (but not
		significant) improvement, concluding that while policy
		alone is useful, it is not sufficient. From that study
		it may be concluded that even when the policy exists,
		without its strict enforcement by journal editors, it
		is not effective. This paper mentions how some authors
		didn't even know about this policy.</p>

	      <p>For some recent discussions in the astronomical
		community in particular, please see Burrell et
		al. (<a href="https://arxiv.org/abs/1901.00143">2019</a>),
		Shamir et
		al. (<a href="https://arxiv.org/abs/1802.00552">2018</a>),
		Oishi et
		al. (<a href="https://arxiv.org/abs/1801.08200">2018</a>)
		and, Allen et
		al. (<a href="https://arxiv.org/abs/1801.02094">2018</a>). This
		is of course not a new problem, discussed as far back
		as Roberts
		(<a href="https://www.sciencedirect.com/science/article/pii/0010465569900113">1969</a>). Gentleman
		and Temple Lang
		(<a href="https://biostats.bepress.com/bioconductor/paper2/">2004</a>)
		introduced the concept of a "research compendium"
		including the text, code and data of a research
		project paper in what they call a "dynamic document"
		which is based
		on <a href="https://en.wikipedia.org/wiki/Literate_programming">Literate
		programming</a>. A nice discussion on the advantages
		of literate programming is also given
		by <a href="http://rlhick.people.wm.edu/posts/reproducible-research.html">
		Rob Hicks</a>. The
		"<a href="https://www.ucpress.edu/book.php?isbn=9780520294752"><i>The
		Practice of Reproducible Research</i></a>" book (also
		available freely
		in <a href="https://github.com/BIDS/practice-repro-research/blob/master/SUMMARY.md">GitHub</a>)
		also provides a good review of reproducible methods
		and solutions. More recently, Hinsen
		(<a href="https://peerj.com/articles/cs-158/">2018</a>)
		also gives a very thorough and fundamental discussion
		of this this situation and provides an interesting
		solution, mainly applied to analytic processes
		(continuous mathematics, not data analysis which often
		deals with discrete elements).</p>

	      <p>This critical issue is also being discussed outside
		of the scientific community. For
		example <a href="https://www.nytimes.com/2018/11/19/science/science-research-fraud-reproducibility.html">this
		2018 essay</a>,
		or <a href="https://www.nytimes.com/2016/08/28/opinion/sunday/do-you-believe-in-god-or-is-that-a-software-glitch.html?_r=0">this
		2016 analysis</a> in the New York Times. The former is
		mainly based on a reproducible experiment
		design. While the latter shows how the lack of
		interest in reproducing other people's results (and
		only trying to continue the biased methods) can grow
		into absurd claims (that are widely cited and used by
		the scientific community).</p>

	      <p>Here, a complete and working solution/template to
		doing research in a fully reproducible manner is
		proposed. We then discuss how useful this proposed
		system can be during the research and also after its
		publication. This system has been implemented and is
		evolving in <a href="reproducible-research.html">my
		own
		research</a>. The <a href="https://www.gnu.org/software/gnuastro/">GNU
		Astronomy Utilities</a> (a collection of software for
		astronomical data analysis) was also created (and
		improved) in parallel with my research to provide the
		low-level analysis tools that were necessary for
		maximal modularity/reproducibility. Please see
		the <a href="https://www.gnu.org/software/gnuastro/manual/html_node/Science-and-its-tools.html">Science
		and its tools</a> section of the Gnuastro book for
		further discussion on the importance of free software
		and reproducibility.</p>

	      <p>Some <a href="pdf/reproducible-paper.pdf">slides</a>
		are also available with figures to help demonstrate
		the concept more clearly and supplement this page.</p>

	      <h2>Implementation</h2>
	      <p>All
		the <a href="https://gitlab.com/makhlaghi/reproducible-paper-dependencies">necessary
		software (dependencies)</a> to do (and reproduce) the
		analysis
		are <a href="https://en.wikipedia.org/wiki/Free_software">free
		software</a>. They are automatically downloaded, built
		and locally installed at the configuration step to
		allow very good control of the working environment
		(highly independent of the host operating system). The
		installation of dependencies is done locally (not
		affecting the host operating system and not needing
		root/administrator access). This also enables working
		on different projects with (possibly) different
		versions of the same software/dependency.</p>

	      <p>All the processing steps in the proposed reproduction
		pipeline are managed
		through <a href="https://en.wikipedia.org/wiki/Makefile">Makefile</a>s. Makefiles
		are arguably the simplest way to define dependency
		between various steps and run independent steps in
		parallel when necessary (to improve speed and thus be
		more creative).</p>

	      <p>When <a href="https://en.wikipedia.org/wiki/Batch_processing">batch
		processing</a> is necessary (no manual intervention,
		as in a reproduction pipeline),
	        <a href="https://en.wikipedia.org/wiki/Shell_script">shell
		scripts</a> usually come to mind. However, the problem
		with scripts for a scientific reproduction pipeline is
		the complexity. A script will start from the top/start
		every time it is run. So if you have gone through 90%
		of a research project and want to run the remaining
		10% that you have newly added, you have to run the
		whole script from the start again and wait until you
		see the effects of the last few steps (for the
		possible errors, or better solutions and etc).</p>

	      <p>The <a href="https://en.wikipedia.org/wiki/Make_(software)">Make</a>
		paradigm, on the other hand, starts from the end: the
		final target. It builds a dependency tree to find
		where it should actually start each time it is
		run. Therefore, in the scenario above, a researcher
		that has just added the final 10% of steps of her
		research to her Makefile, will only have run those
		extra steps. This greatly speeds up the processing
		(enabling creative changes), while keeping all the
		dependencies clearly documented (as part of
		the <a href="https://en.wikipedia.org/wiki/Make_(software)">Make</a>
		language), and most importantly enabling full
		reproducibility. Since the dependencies are also
		clearly demarcated, Make can identify independent
		steps and run them in parallel (further speeding up
		the process). Make was designed for this purpose and
		it is how huge projects like
		all <a href="https://en.wikipedia.org/wiki/Unix-like">Unix-like</a>
		operating systems (including GNU/Linux or Mac OS
		operating systems) and their core components are
		built.</p>

	      <p>The output of a research is either some numbers, a
                plot, or more formally, a report/paper. Therefore the
                output (final Makefile target) of the reproduction
                pipeline described here is a PDF that is created by
                <a href="https://en.wikipedia.org/wiki/LaTeX">LaTeX</a>. Each
                step stores the necessary numbers, tables, or images
                that need to go into the final report into separate
                files. In particular, any processed number that must
                be included within the text of the final PDF is
                actually
                a <a href="https://en.wikibooks.org/wiki/LaTeX/Macros">LaTeX
                macro</a>. Therefore, when any step of the processing
                is updated/changed, the numbers and plots within the
                text will also correspondingly change.</p>

	      <p>The whole reproduction pipeline (Makefiles, all the
 		configuration files of the various software and also
 		the necessary source files) are plain text files, so
 		they don't take much space (usually less than 1
 		mega-byte). Therefore the whole pipeline can be
 		packaged with the LaTeX source of the paper and
 		uploaded to
 		<a href="https://en.wikipedia.org/wiki/ArXiv">arXiv</a>
 		when the paper is published. arXiv is arguably one of
 		the most important repositories for papers in many
 		fields (not just astronomy) with many mirrors around
 		the world. Therefore, the fact that the whole
 		processing is engraved in arXiv along with the paper
 		is arguably one of the best ways to ensure that it is
 		kept for the future.</p>

	      <p>A <a href="https://gitlab.com/makhlaghi/reproducible-paper">fully
		  working template</a> based on the proposed method
		  above has been defined which can be easily
		  configured for any
		  research. The <a href="https://gitlab.com/makhlaghi/reproducible-paper/blob/master/README.md"><code>README.md</code></a>
		  file in that template gives full information how to
		  configure and adapt the pipeline to your research
		  needs. Please try it out and share your thoughts to
		  make it work more robustly and integrate better in
		  the future.</p>

	      <p>The implementation introduced here was independently
		designed, but has many similarities with the "Research
		compendium" concept of Gentleman and Temple Lang
		(<a href="https://biostats.bepress.com/bioconductor/paper2/">2004</a>)
		or Basu
		(<a href="https://doi.org/10.32388/649864">2018</a>). The
		major difference is that here, we don't
		use <a href="https://en.wikipedia.org/wiki/Literate_programming">Literate
		programming</a>. The basic reason is modularity and
		abstractions. At the first/highest level, readers just
		want a general introduction to the whole research in
		an easy to read and less technical manner. Only when
		they are very interested in the result will they look
		into the details of the analysis (figures in a
		paper). Only once that further raises their curiosity
		will they actually invest the time and energy to look
		into the raw processing steps. A similar process
		happens during the definition phases of a research
		project by its authors. Following this logic, this
		pipeline is designed on the principle of maintaining
		the research project as two separate/modular
		components: 1) its coding/scripts/software and 2)
		plain text descriptions used to generate the final
		paper/report.</p>

	      <p>This modularity allows the use of optimized tools for
		each step separately without any intermediate tool
		(extra dependency) to <i>weave</i> or <i>tangle</i>
		the separate components. For example you can only
		find, test and fix a bug in the coding step on the
		tangled output. After fixing it there, if you forget
		to re-implement it in the tangled source, the next
		tangle will re-write all the changes. Human-readable
		text within code is indeed very important, but by
		definition, such low-level, or technical, description
		is only interesting/useful after the reader has
		obtained the generic view. Comments in the code serve
		this purpose (low-level documentation) wonderfully,
		thus easily complementing the high-level paper's
		descriptions in the LaTeX source without the need for
		any intermediate tool.</p>

	      <h2>Practical benefits</h2>
	      <p>Reproducibility is the main reason this system was
		designed. However, a reproduction pipeline like this
		also has lots of practical benefits that are listed
		below. Of course, for all the situations above to be
		maximally effective, the scripts have to be
		nicely/thoroughly <a href="https://en.wikipedia.org/wiki/Comment_(computer_programming)">commented</a>
		for easy human (not just computer) readability.</p>

	      <h3>While doing the research</h3>
	      <ul>
		<li>Other team members in a research project can
		  easily run/check/understand all the steps written by
		  other members and find possibly better ways of
		  reaching the result or implement their part of the
		  research in a better fashion.</li>

		<li>During the research project, it might happen that
		  one of the parameters is decided to be changed or a
		  new version of some of the used software is
		  released. With this system, updating all the numbers
		  and plots in the paper is as simple as running a
		  make command and the authors don't have to worry
		  about part of the paper having the old configuration
		  and the other part with the new
		  configuration. Manually trying to change everything
		  in the text will be prone to errors.</li>

		<li>If the referee asks for another set of parameters,
		  they can be immediately replaced and all the plots
		  and numbers in the paper will be correspondingly
		  updated.</li>

		<li>Enabling <a href="https://en.wikipedia.org/wiki/Version_control">version
                  control</a> (for example
                  with <a href="https://en.wikipedia.org/wiki/Git">Git</a>)
                  on the contents of this reproduction pipeline will
                  make it very simple to revert everything back to a
                  previous state. This will enabling researchers to
                  experiment more with alternative methods and new
                  ideas, even in the middle of an on-going
                  research. <a href="https://gitlab.com">GitLab</a>
                  enables free private repositories which is very
                  useful for collaborations to privately share their
                  work prior to its publication.</li>

		<li>The authors can allow themselves to forget the
		  details and keep their mind open to new
		  possibilities. In any situation they can simply
		  refer back to these scripts and see exactly what
		  they did. This will enable researchers to be more
		  open to learning/trying new methods without worrying
		  about loosing/forgetting the details of their
		  previous work.</li>
	      </ul>

	      <h3>After publication</h3>
	      <ul>
		<li>Other scientists can modify the parameters or the
		  steps in order to check the effect of those changes
		  on the plots and reported numbers and possibly find
		  enhancements/problems in the result.</li>

		<li>It serves as an excellent repository for students,
		  or scientists with different specialties to master
		  the art of data processing and analysis in this
		  particular sub-field. By removing this barrier, it
		  will enable the mixture of the experiences of the
		  different fields, potentially leading to new
		  insights and thus discoveries.</li>

		<li>By changing the basic input parameters, the
		  readers can try the exact same steps on other
		  data-sets and check the result on the same text that
		  they have read and have become familiar with.</li>
	      </ul>

	      <h2>Similar attempts</h2>
	      <p>Fortunately other scientists have also made similar
		attempts at reproduction. There is also a nice
		technical guideline and discussion is also available
		in <a href="https://www.nature.com/articles/d41586-018-05990-5">Nature
		(August 20, 2018)</a>. Also see Gentleman and Temple
		Lang
		(<a href="https://biostats.bepress.com/bioconductor/paper2/">2004</a>,
		with some examples
		in <a href="https://research-compendium.science">research-compendium.science</a>),
		and Hinsen
		(<a href="https://peerj.com/articles/cs-158/">2018</a>). A
		rich collection of resources is also available
		in <a href="https://reproduciblescience.org">reproduciblescience.org</a>. Two
		MOOCs (massive open online courses) are also
		available: one
		in <a href="https://opensciencemooc.eu/">opensciencemooc.eu</a>,
		and the other
		by <a href="https://learninglab.inria.fr/en/mooc-recherche-reproductible-principes-methodologiques-pour-une-science-transparente/">INRIA</a>.</p>
	      <p>Other (generally) similar projects that provide a
		tutorial for reproducible research that I have found
		so far are listed below:</p>
	      <ul>
		<li>Pinga-lab's <a href="https://github.com/pinga-lab/paper-template">paper
		template</a>. Similar to the proposed solution here,
		it suggests using Makefiles. However, for the
		necessary software, it uses Conda to install software
		(not needed here) and needs a Python environment (not
		needed here). Similar to this project, it goes all the
		way to building the PDF paper and uses LaTeX variables
		to update results.</li>
	      </ul>


	      <h2>Reproducible papers</h2>

	      <p>A list of papers that I have seen so far is available
		below (mostly in my own field of astronomy). Hopefully
		this list will greatly expand in the near future. If
		you know of any other attempts, please let me know so
		I can update the list.</p>

	      <ul>
		<li><a href="https://barbagroup.github.io/essential_skills_RRC/">Essential
		    skills for reproducible research computing</a>:
		  Contents of a workshop on reproducible
		  research, <a href="https://figshare.com/articles/Introduction_to_Computational_Reproducibility_and_why_we_care_/4509419">some
		slides</a> are also provided, along with a
		  nice <a href="https://hackernoon.com/barba-group-reproducibility-syllabus-e3757ee635cf">literature
		    review</a> and
		  a <a href="http://lorenabarba.com/gallery/reproducibility-pi-manifesto/">Manifesto</a>. In
		  general <a href="http://lorenabarba.com/">Lorena
		    Barba</a> (author of the links here) and her team are
		  making some great progress in this regard.</li>

                <li>Jones et al. (2019,
                arXiv: <a href="https://arxiv.org/abs/1910.03420">1910.03420</a>). This
                paper also follows a reproducible structure and has a
                nice appendix in the end, discussing its importance
                and their implementation method.</li>

                <li>Akhlaghi (2019,
                arXiv: <a href="https://arxiv.org/abs/1909.11230">1909.11230</a>). This
                is the first paper to fully implement the more
                advanced features of the pipeline presented here. All
                the analysis steps (as well as input data and software
                tarball sources, their MD5 checksums, instructions to
                build the software, and how to run them and produce
                the final paper) are uploaded to arXiv with the paper,
                they are also backed up
                on <a href="https://doi.org/10.5281/zenodo.3408481">zenodo.3408481</a>.</li>

		<li>Roukema et al. (2019,
		arXiv: <a href="https://arxiv.org/abs/1902.09064">1902.09064</a>). The
		processing steps are available
		on <a href="https://bitbucket.org/broukema/1902.09064">Bitbucket</a>. This
		is a very interesting pipeline because it also has
		instructions to install all the necessary software
		(assuming a Debian-based operating system, requiring
		root permissions for basic tools). </li>

		<li>Kulkarni et al. (2018,
		arXiv: <a href="https://arxiv.org/abs/1807.09774">1807.09774</a>). (as
		of July 2018, submitted to) Monthly Notices of the
		Royal Astronomical Society. The data and processing to
		derive the results is available
		on <a href="https://github.com/gkulkarni/QLF">GitHub</a>.</li>

		<li>Paxton et
		  al. (<a href="https://doi.org/10.3847/1538-4365/aaa5a8">2018</a>,
		  arXiv:<a href="https://arxiv.org/abs/1710.08424">1710.08424</a>). Astrophysical
		  Journal Supplement Series, 234:34. Their
		  reproduction system is described in Appendix D6, as
		  part of a larger system for all research using the
		  <a href="http://mesa.sourceforge.net/">MESA</a>
		  software.</li>

		<li>Parviainen et
		  al. (<a href="http://dx.doi.org/10.1051/0004-6361/201526313">2016</a>, arXiv:<a href="https://arxiv.org/abs/1510.04988">1510.04988</a>)
		  Astronomy & Astrophysics, 585, A114. The
		  reproduction scripts are available
		  on <a href="https://github.com/hpparvi/Parviainen-2015-TrES-3b-OSIRIS">GitHub</a>.</li>
		<li>Moravveji et
		  al. (<a href="https://doi.org/10.1093/mnrasl/slv142">2016</a>,
		  arXiv:<a href="https://arxiv.org/abs/1509.08652">1509.08652</a>). Monthly
		  Notices of the Royal Astronomical Society, 455,
		  L67. The reproduction information is available on
		  <a href="https://bitbucket.org/ehsan_moravveji/op_mono/wiki/Home">Bitbucket</a>.</li>
		<li>VanderPlas and Ivezic
		(<a href="http://dx.doi.org/10.1088/0004-637X/812/1/18">2015</a>,
		arXiv:<a href="https://arxiv.org/abs/1502.01344">1502.01344</a>)
		Astrophysical Journal, 812, 1. The reproduction
		pipeline is available
		on <a href="https://github.com/jakevdp/multiband_LS">GitHub</a>. Their
		implementation is described in
		these <a href="https://zenodo.org/record/49577/files/In_Defense_of_Extreme_Openness.pdf">nice
		slides</a>. Just as a comment on the slides:
		<a href="https://gitlab.com">GitLab</a> allows
		unlimited private repositories for free, along with
		the capability to host Git repos on your own servers.
		Therefore, the paper's Git repository can remain
		private until the paper is published.</li>
		<li>Robitaille et
		  al. (<a href="http://dx.doi.org/10.1051/0004-6361/201219073">2012</a>, arXiv:<a href="https://arxiv.org/abs/1208.4606">1208.4606</a>)
		  Astronomy & Astrophysics, 545, A39. The reproduction
		  scripts are available
		  on <a href="https://github.com/hyperion-rt/paper-galaxy-rt-model">GitHub</a>.</li>
	      </ul>


	      <p>Since there is no adopted/suggested standard yet,
		each follows a different method which is not exactly
		like this paper's reproduction pipeline. This is still
		a new concept and thus such different approaches are
		great to make the concept more robust. Besides the
		suggested style here, please have a look at these
		methods too and adopt your own style (what you find
		the best in each) and share it.</p>


	      <h2>Non-reproducible papers</h2>

	      <p>Unfortunately not all researchers have the view
		described above on scientific methodology. In their
		view to science, only results are
		important. Therefore, a vague description (in the text
		of the paper) is enough and the exact method can be
		kept as a trade secret. To this class of researchers,
		doing science is similar to doing magic tricks (where
		the magician's methods are his/her trade secrets, and
		their audience only want results/entertainment). Here
		is a list of such papers that I have come across so
		far:</p>

	      <ul>
		<li>Zhang et al. 2018
		(<a href="https://doi.org/10.1038/s41586-018-0196-x">Nature</a>,
		June 4th,
		2018, <a href="https://arxiv.org/abs/1806.01280">arXiv:1806:01280</a>). In
		the "Code availability" section close to the end
		(which they had to add due to Nature's new policy
		on <a href="https://www.nature.com/authors/policies/availability.html#code">Availability
		of computer code and algorithm</a>), they blatantly
		write: "<i>We opt not to make the code used for the
		chemical evolution modeling publicly available because
		it is an important asset of the researchers’
		toolkits</i>".</li>
	      </ul>

	      <h2>Data acquisition</h2>
	      <p>The discussions above were mainly concerned with the
		reproducible <i>analysis</i> of data after it is
		acquired (saved as raw data on a computer, for example
		tables or images). Fortunately in astronomy, raw data
		from major telescopes become publicly available a year
		or two after their acquisition and all the meta-data
		(telescope settings or observation
		strategies/protocols) are also stored in the datasets
		(usually as <a href="https://fits.gsfc.nasa.gov/">FITS
		format</a> header keywords). So in astronomy, full
		information on data acquisition isn't usually a
		problem.</p>

	      <p>In other sciences, especially when such major data
		acquisition facilities don't exist and experiments are
		done in local labs with different instruments, it is
		very important to keep the exact data acquisition
		process documented and versioned. One interesting
		solution to these can be places such
		as <a href="https://www.protocols.io/">protocol.io</a>. Similarly,
		a "Minimal information reporting" concept was
		introduced for nanotechnology experiment design and
		reporting by Faria et
		al. <a href="https://www.nature.com/articles/s41565-018-0246-4">(2018)</a>. <a href="https://www.nature.com/articles/s41565-019-0496-9">Nature
		Nanotechnology</a> then published a nice discussion on
		this issue by many researchers. When the hardware are
		already known, there are also pipelines to standardize
		the experiment design, for example Hafner et
		al. (<a href="https://www.ncbi.nlm.nih.gov/pubmed/28628201">2017</a>)</p>

	      <p>Of course, higher-level analysis, that starts from
		the moment the researcher(s) work on files in a
		computer rather than hardware, is still done on raw
		data to achieve a result. Therefore as described
		above, to have a reproducible/scientific result, the
		raw acquired data and the detailed processing steps
		done on it must be published along with the
		result/paper independently of how it was
		acquired. Hence reproducible data acquisition
		protocols are indeed necessary, but not sufficient
		step for a fully reproducible result.</p>

	      <h2>Other related pages</h2>
	      <p>There are many links to various resources during the
	      text of the article above. Below are some links to other
	      similar work on this subject.</p>
	      <ul>
		<li>Arizona State University
		library, <a href="https://libguides.asu.edu/openaccess/opendata-science">open
		data/science</a> page. This page also contains links
		to useful resources for doing reproducible science.</li>
	      </ul>

	      <h2>Acknowledgments</h2>

	      <p>Mohammad-reza Khellat, Alan Lefor, Alejandro Serrano
		Borlaff kindly provided very useful comments or found
		bugs in the suggested reproduction pipeline. Alice
		Allen, Mosè Giordano, Gérard Massacrier, Ehsan
		Moravveji, Peter Mitchell, Boud Roukema, Yahya
		Sefidbakht, Vicky Steeves, David Valls-Gabaud and Paul
		Wilson (in alphabetical order) kindly informed me of
		some of the links mentioned here.</p>
	    </section>
	  </article>

	</main>


	<!-- Sidebar -->
	<div class="sidebar">

	  <aside role="complementary" class="importantaside">
	    <h1>REPRODUCIBLE SCIENCE</h1>
	    <p>See <a href="https://arxiv.org/abs/2006.03018">arXiv:2006.03018</a>
	    describing the necessity of reproducible research and that
	    they must consider longevity.</p>
	  </aside>

	  <aside role="complementary">
	    <h1>MANEAGE (MANAGE DATA LINEAGE)</h1>
	    <p>Maneage (<a href="https://maneage.org">maneage.org</a>)
	    is a highly customizable framework, allowing full control
	    over data lineage that is already used in the publications
	    of this page.</p>
	  </aside>

	  <aside role="complementary">
	    <h1>SUMMARY SLIDES</h1>
	    <p>Some <a href="pdf/reproducible-paper.pdf">summary
	    slides</a> are also available with figures to help
	    demonstrate the purpose/concept.</p>
	  </aside>

	</div>

	<!-- End of the main and sidbar container -->
      </div>


      <!-- Page footer -->
      <footer role="contentinfo">
	<address>Centro de Estudios de Física del Cosmos de Aragón, Plaza San Juan 1, Planta 2, Teruel, Spain.</address>
	<copyright>Copyright &copy; 2016-2025 Mohammad Akhlaghi
	  (<a href="mailto:Mohammad
		    Akhlaghi<mohammad|at|akhlaghi.org>">mohammad::at::akhlaghi.org</a>).</copyright>
      </footer>

    </div>
  </body>

</html>
