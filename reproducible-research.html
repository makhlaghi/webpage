<!DOCTYPE html>
<!--
  Mohammad Akhlaghi's personal webpage.
  Copyright (C) 2018-2025 Mohammad Akhlaghi <mohammad@akhlaghi.org>

  This HTML source is free software: you can redistribute it and/or
  modify it under the terms of the GNU General Public Licence as
  published by the Free software foundation, either version 3 of the
  License, or (at your option) any later version.

  This HTML source is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  A copy of the GNU General Public License is available at
  <http://www.gnu.org/licenses>.
-->

<html lang="en-US">

  <head>
    <title>Reproducible papers -- Mohammad Akhlaghi -- Centro de Estudios de Física del Cosmos de Aragón</title>

    <meta charset="UTF-8">
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8">

    <link rel="shortcut icon" href="./img/cefca-icon.png" />

    <meta name="viewport"
	  content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/fonts.css">
    <link rel="stylesheet" href="css/base.css">
    <link rel="stylesheet" media="only screen and (min-width: 32em)"
	  href="css/wide.css">
  </head>

  <body>
    <div class="page">



      <!-- Page top and navigation -->
      <header role="banner">
	<nav role="navigation">
	  <ul>
	    <li>MOHAMMAD AKHLAGHI</li>
	    <li><a href="index.html">Welcome</a></li>
	    <li><a href="blog.html">Blog</a></li>
	  </ul>
	</nav>
      </header>



      <!-- Container keeping main and sidebar -->
      <div class="container clearfix">


	<!-- Main body of page -->
	<main role="main">
	  <article>
	    <section class="title-abstract">
	      <h1>Research reproduction pipelines</h1>

	      <p> This page contains my reproducible projects that has
		already been published using Maneage (<i>Man</i>aging
		data lin<i>eage</i>, hosted
		at <a href="https://maneage.org">https://maneage.org</a>),
		or its earlier incarnations. See Akhlaghi et
		al. (<a href="https://arxiv.org/abs/2006.03018">2021</a>,
		arXiv:2006.03018) for an introduction to this
		concept.</p>

	      <p>All the necessary software used in the research below
		are <a href="https://en.wikipedia.org/wiki/Free_software">free
		software</a>, enabling any curious scientist to easily
		study and experiment on the source code of the
		software producing the results. All the numbers and
		plots of the paper where I am the principal author (or
		the parts that I contributed to in other papers) can
		be exactly reproduced through the scripts listed
		here. All the scripts within the pipeline (that call
		the separate programs) are also heavily commented to
		explain every step for the human reader
		thoroughly.</p>

	      <p>The paper or section number identification is a link
		to the Gitlab repository keeping the pipeline. The
		links following the paper's title point to the
		published paper. </p>

	      <dl>

		<dt><a href="https://gitlab.com/makhlaghi/maneage-paper">Akhlaghi
		    et al. (2020)</a>: <i>Towards Long-term and
		    Archivable Reproducibility</i>. Published in <a href="https://ui.adsabs.harvard.edu/abs/2021CSE....23c..82A">CiSE 23c 82A</a>,
		    arXiv:<a href="https://arxiv.org/abs/2006.03018">2006.03018</a>.</dt>
		<dd>
		  <p>This paper if fully devoted to Maneage and its
		    founding criteria. It highlights the serious
		    longevity-related problems of the tools used in most
		    existing reproducible solutions, and describes how
		    Maneage is able to address them. All the necessary
		    reproducible supplements to this paper are published
		    in <a href="https://doi.org/10.5281/zenodo.3408481">zenodo.3408481</a>.</p>

		  <p>One interesting by-product of the reproducibility
		    that is implemented here is the link at the end of
		    the caption in Figure 1. It points directly to the
		    dataset that produced the plot, hosted on Zenodo
		    (not a repository I control and might mistakenly
		    delete in a few years)! With the data set having
		    the same Git hash as the paper, as well as other
		    metadata and copyright statements. Reading/using
		    other papers would be so easy if this was more
		    practiced. Through Maneage, I tried to simplify
		    implementation of such features in derived
		    projects. </p>
		</dd>

		<dt><a href="https://gitlab.com/makhlaghi/iau-symposium-355">Akhlaghi
		(2019)</a>: <i>Carving out the low surface brightness
		universe with NoiseChisel</i>, Invited talk at IAU
		Symposium 355. Published in
		arXiv:<a href="https://arxiv.org/abs/1909.11230">1909.11230</a>.</dt>
		<dd>
		  <p>This is the first paper to fully implement the
		  modern implementation of Maneage (that also installs
		  its own software). It is primarily focused on all
		  the improvements to the NoiseChisel software over
		  the last four years since its initial
		  submission. All the necessary reproducible
		  supplements to this paper are published
		  in <a href="https://doi.org/10.5281/zenodo.3408481">zenodo.3408481</a>.</p>
		</dd>

		<dt><a href="https://git-cral.univ-lyon1.fr/mohammad.akhlaghi/muse-udf-photometry-astrometry">Section
		  4</a>
		  and <a href="https://git-cral.univ-lyon1.fr/mohammad.akhlaghi/muse-udf-origin-only-hst-magnitudes">Section
		  7.3</a> of Bacon et al. (2017): <i>The MUSE Hubble
		  Ultra Deep Field Survey I. Survey description, data
		  reduction, and source
		  detection</i>. Published in <a href="https://doi.org/10.1051/0004-6361/201730833">A&amp;A 608,
		  A1</a>,
		  arXiv: <a href="https://arxiv.org/abs/1710.03002">1710.03002</a></dt>
		<dd>
		  <p>In this project, I was in charge of the analysis
		    behind the two sections mentioned
		    above. <a href="https://www.aanda.org/articles/aa/full_html/2017/12/aa30833-17/aa30833-17.html#S14">Section
		    4</a> discusses the astrometry and photometry
		    checks that we did on pseudo-broad-band images
		    created from MUSE cubes to compare the results
		    with broad-band HST images. The dataset necessary
		    for this section (generally, everything necessary
		    to exactly reproduce the results) is available
		    in <a href="https://doi.org/10.5281/zenodo.1163746">Zenodo</a>. <a href="https://www.aanda.org/articles/aa/full_html/2017/12/aa30833-17/aa30833-17.html#S23">Section
		    7.3</a> focuses on how a broad-band segmentation
		    map was assigned to objects that could only be
		    detected in the MUSE cube with the ORIGIN software
		    and the corresponding broad-band measurements were
		    made. The reproduction pipeline and all the
		    necessary software and data of this section are
		    also uploaded
		    to <a href="https://doi.org/10.5281/zenodo.1164774">Zenodo</a>.
		  </p>
		</dd>

		<dt><a href="https://gitlab.com/makhlaghi/NoiseChisel-paper">Akhlaghi
		  &amp; Ichikawa (2015)</a>: <i>Noise Based Detection
		  and Segmentation of Nebulous
		  Objects</i>. Published in <a href="http://iopscience.iop.org/article/10.1088/0067-0049/220/1/1">ApJS
		  220, 1</a>,
		  arXiv: <a href="https://arxiv.org/abs/1505.01664">1505.01664</a></dt>
		<dd><p>This paper is a definition of the new
		noise-based detection and segmentation paradigm and is
		my first scientific paper. My work to make a fully
		reproducible scientific research was born while
		writing this paper. My softwares project
		(<a href="https://www.gnu.org/software/gnuastro/">GNU
		Astronomy Utilities</a>) was also born from this
		    paper.</p></dd>


	      </dl>

	    </section>
	  </article>

	</main>


	<!-- Sidebar -->
	<div class="sidebar">

	  <aside role="complementary" class="importantaside">
	    <h1>REPRODUCIBLE SCIENCE</h1>
	    <p>See <a href="https://arxiv.org/abs/2006.03018">arXiv:2006.03018</a>
	    describing the necessity of reproducible research and that
	    they must consider longevity.</p>
	  </aside>

	  <aside role="complementary">
	    <h1>MANEAGE (MANAGE DATA LINEAGE)</h1>
	    <p>Maneage (<a href="https://maneage.org">maneage.org</a>)
	    is a highly customizable framework, allowing full control
	    over data lineage that is already used in the publications
	    of this page.</p>
	  </aside>

	  <aside role="complementary">
	    <h1>SLIDES ON REPRODUCIBILITY</h1>
	    <p>Some <a href="http://maneage.org/pdf/slides-intro.pdf">slides</a>
	    are also available for a more information
	    introduction.</p>
	  </aside>

	</div>

	<!-- End of the main and sidbar container -->
      </div>


      <!-- Page footer -->
      <footer role="contentinfo">
	<address>Centro de Estudios de Física del Cosmos de Aragón, Plaza San Juan 1, Planta 2, Teruel, Spain.</address>
	<copyright>Copyright &copy; 2016-2025 Mohammad Akhlaghi
	  (<a href="mailto:Mohammad
		    Akhlaghi<mohammad|at|akhlaghi.org>">mohammad::at::akhlaghi.org</a>).</copyright>
      </footer>

    </div>
  </body>

</html>
